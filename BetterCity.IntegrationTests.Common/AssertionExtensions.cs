using System.Net;
using System.Net.Http;
using FluentAssertions;
using FluentAssertions.Primitives;

namespace BetterCity.IntegrationTests.Common
{
    public static class AssertionExtensions
    {
        public static AndConstraint<ObjectAssertions> ShouldBeOk(this HttpResponseMessage response)
            => response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
}