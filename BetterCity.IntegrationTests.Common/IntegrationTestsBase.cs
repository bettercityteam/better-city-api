using System.Net.Http;
using Microsoft.AspNetCore.Mvc.Testing;

namespace BetterCity.IntegrationTests.Common
{
    public class IntegrationTestsBase<TFactory, TStartup>
    where TFactory : WebApplicationFactory<TStartup>, new()
    where TStartup : class
    {
        protected readonly TFactory Factory = new();
        protected readonly HttpClient Client;

        public IntegrationTestsBase()
        {
            Client = Factory.CreateClient();
        }
    }
}