using System;
using System.Collections.Generic;

namespace BetterCity.Domain
{
    public class PlaceGeometry
    {
        public Guid Id { get; set; }
        
        public Guid PlaceId { get; set; }
                
        public Point[] BoundingPolygon { get; set; }
        
        public List<Waypoint> Waypoints { get; set; }
        
    }
}