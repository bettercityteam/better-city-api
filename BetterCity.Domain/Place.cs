using System;
using System.Collections.Generic;

namespace BetterCity.Domain
{
    public class Place
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }
        
        public string Summary { get; set; }

        public List<PlaceCategory> Categories { get; set; }
        
    }
}