using System;

namespace BetterCity.Domain
{
    public class PlaceCategory
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }
        
        public string ImageId { get; set; }
    }
}