using System;

namespace BetterCity.Domain
{
    public class Waypoint : Point
    {
        public Guid Id { get; set; }
        
        public double RadiusMeters { get; set; }
        
        public string Name { get; set; }
    }
}