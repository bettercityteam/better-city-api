namespace BetterCity.Domain
{
    public class Point
    {
        public double Longitude { get; set; }
        
        public double Latitude { get; set; }
    }
}