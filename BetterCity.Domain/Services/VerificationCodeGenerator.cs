using System;

namespace BetterCity.Domain.Services
{
    public static class VerificationCodeGenerator
    {
        public static string Generate() => new Random().Next(100000, 999999).ToString();
    }
}