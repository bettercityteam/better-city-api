using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace BetterCity.Employee.Infrastructure
{
    public class BetterCityEmployeeDbContextFactory : IDesignTimeDbContextFactory<BetterCityEmployeeDbContext>
    {
        public BetterCityEmployeeDbContext CreateDbContext(string[] args)
        {
            if (args.Length == 0)
                args = new[] {"dbmigration.settings.json"};

            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(args[0])
                .Build();

            var builder = new DbContextOptionsBuilder<BetterCityEmployeeDbContext>();
            builder.UseSqlServer(configuration.GetConnectionString("Default"));
            return new BetterCityEmployeeDbContext(builder.Options);
        }
    }
}