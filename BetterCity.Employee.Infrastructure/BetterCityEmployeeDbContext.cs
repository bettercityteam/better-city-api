using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BetterCity.Employee.Infrastructure
{
    public class BetterCityEmployeeDbContext : IdentityDbContext
    {
        public BetterCityEmployeeDbContext(DbContextOptions<BetterCityEmployeeDbContext> options)
        : base(options)
        {
        }
    }
}