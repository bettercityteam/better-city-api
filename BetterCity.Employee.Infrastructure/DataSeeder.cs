using System.Linq;
using System.Threading.Tasks;
using BetterCity.Domain;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace BetterCity.Employee.Infrastructure
{
    public class DataSeeder
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public DataSeeder(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public async Task<bool> TrySeedUserAsync(string username, string password)
        {
            var user = new IdentityUser(username);
            var result = await _userManager.CreateAsync(user, password);
            return result.Succeeded;
        }

        public async Task<bool> TryAddToRoleAsync(string username, string role)
        {
            var user = await _userManager.FindByNameAsync(username);
            if (user == null)
                return false;
            var result = await _userManager.AddToRoleAsync(user, role);
            return result.Succeeded;
        }

        public async Task SeedAllRolesAsync()
        {
            if(await _roleManager.Roles.AnyAsync())
                return;
            
            var roles = new[] {Roles.Admin, Roles.Moderator}
                .Select(role => new IdentityRole(role));

            foreach (var role in roles)
            {
                await _roleManager.CreateAsync(role);
            }
        }
    }
}