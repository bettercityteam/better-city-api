using System.Net.Http;

namespace BetterCity.Public.Web.Tests.Fixture
{
    public class IntegrationTestsBase
    {
        protected readonly BetterCityPublicWebApplicationFactory Factory = new();
        protected readonly HttpClient Client;

        public IntegrationTestsBase()
        {
            Client = Factory.CreateClient();
        }
    }
}