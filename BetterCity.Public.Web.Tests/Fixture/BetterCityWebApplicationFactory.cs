using System;
using System.Linq;
using BetterCity.User.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.DependencyInjection;

namespace BetterCity.Public.Web.Tests.Fixture
{
    public class BetterCityPublicWebApplicationFactory : WebApplicationFactory<Startup>
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            var guid = Guid.NewGuid().ToString();
            base.ConfigureWebHost(builder);
            builder.ConfigureServices(services =>
            {
                var descriptor = services.SingleOrDefault(
                    d => d.ServiceType ==
                         typeof(DbContextOptions<BetterCityUserDbContext>));

                services.Remove(descriptor);

                services.AddDbContext<BetterCityUserDbContext>(options =>
                {
                    options.UseInMemoryDatabase(guid).ConfigureWarnings(inMemory => inMemory.Ignore(InMemoryEventId.TransactionIgnoredWarning));
                });
                
                var sp = services.BuildServiceProvider();

                using var scope = sp.CreateScope();
                var context = scope.ServiceProvider.GetRequiredService<BetterCityUserDbContext>();
                context.Database.EnsureCreated();
            });
        }
    }
}