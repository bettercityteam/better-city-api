using System.Linq;
using System.Net;
using System.Threading.Tasks;
using BetterCity.Common.Web;
using BetterCity.Public.Web.Tests.Fixture;
using FluentAssertions;
using Xunit;

namespace BetterCity.Public.Web.Tests
{
    public class SwaggerTests : IntegrationTestsBase
    {
        [Fact]
        public async Task Swagger_json_is_available()
        {
            var response = await Client.GetAsync(SwaggerConstants.JsonPath);
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task Swagger_ui_is_available()
        {
            var response = await Client.GetAsync(SwaggerConstants.ApiPath);
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
    }
}