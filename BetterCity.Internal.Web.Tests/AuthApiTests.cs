using System.Net;
using System.Threading.Tasks;
using BetterCity.IntegrationTests.Common;
using BetterCity.Internal.Web.Tests.Fixture;
using FluentAssertions;
using Xunit;

namespace BetterCity.Internal.Web.Tests
{
    [Collection("Test")]
    public class AuthApiTests : InternalApiIntegrationTestsBase
    {
        [Fact]
        public async Task Sign_in_as_a_seeded_admin_responds_ok()
        {
            var response = await Client.SignInAsDefaultAdminAsync();

            response.ShouldBeOk();
        }

        [Fact]
        public async Task Sign_in_response_contains_cookies_when_sign_in_as_a_seeded_admin()
        {
            var response = await Client.SignInAsDefaultAdminAsync();

            response.ShouldBeOk();
            response.Headers.GetValues("Set-Cookie").Should().NotBeEmpty();
        }

        [Fact]
        public async Task Sign_out_response_has_status_ok_when_sign_in_before()
        {
            _ = await Client.SignInAsDefaultAdminAsync();

            var response = await Client.SignOutAsync();

            response.ShouldBeOk();
        }

        [Fact]
        public async Task Sign_out_response_has_status_404_not_found_when_client_not_signed_in()
        {
            var response = await Client.SignOutAsync();

            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }
    }
}