using System;
using System.Linq;
using System.Threading.Tasks;
using BetterCity.Domain;
using BetterCity.Employee.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace BetterCity.Internal.Web.Tests.Fixture
{
    public class BetterCityInternalWebApplicationFactory : WebApplicationFactory<Startup>
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            var guid = Guid.NewGuid().ToString();
            base.ConfigureWebHost(builder);
            builder.ConfigureServices(services =>
            {
                var descriptor = services.SingleOrDefault(
                    d => d.ServiceType ==
                         typeof(DbContextOptions<BetterCityEmployeeDbContext>));

                services.Remove(descriptor);

                services.AddDbContext<BetterCityEmployeeDbContext>(options =>
                {
                    options.UseInMemoryDatabase(guid).ConfigureWarnings(inMemory => inMemory.Ignore(InMemoryEventId.TransactionIgnoredWarning));
                });
                
                var sp = services.BuildServiceProvider();

                using var scope = sp.CreateScope();
                var context = scope.ServiceProvider.GetRequiredService<BetterCityEmployeeDbContext>();
                context.Database.EnsureCreated();
            });
        }

        protected override IHost CreateHost(IHostBuilder builder)
        {
            var host = base.CreateHost(builder);
            SeedDataAsync(host).Wait();
            return host;
        }

        private static async Task SeedDataAsync(IHost host)
        {
            using var scope = host.Services.CreateScope();
            var seeder = scope.ServiceProvider.GetRequiredService<DataSeeder>();
            await seeder.SeedAllRolesAsync();
            await seeder.TrySeedUserAsync(DefaultAdminCredentials.Username, DefaultAdminCredentials.Password);
            await seeder.TryAddToRoleAsync(DefaultAdminCredentials.Username, Roles.Admin);
        }
    }
}