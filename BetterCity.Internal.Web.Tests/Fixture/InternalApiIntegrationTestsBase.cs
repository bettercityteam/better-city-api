using BetterCity.IntegrationTests.Common;

namespace BetterCity.Internal.Web.Tests.Fixture
{
    public abstract class InternalApiIntegrationTestsBase : IntegrationTestsBase<BetterCityInternalWebApplicationFactory, Startup>
    {
        
    }
}