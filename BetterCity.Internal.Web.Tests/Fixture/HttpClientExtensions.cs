using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace BetterCity.Internal.Web.Tests.Fixture
{
    public static class HttpClientExtensions
    {
        public static Task<HttpResponseMessage> SignInAsync(this HttpClient client, string username, string password)
        {
            var body = new
            {
                Username = DefaultAdminCredentials.Username,
                Password = DefaultAdminCredentials.Password
            };

            return client.PostAsJsonAsync("/api/v1/auth/signin", body);
        }

        public static Task<HttpResponseMessage> SignInAsDefaultAdminAsync(this HttpClient client)
            => client.SignInAsync(DefaultAdminCredentials.Username, DefaultAdminCredentials.Password);

        public static Task<HttpResponseMessage> SignOutAsync(this HttpClient client)
            => client.PostAsync("/api/v1/auth/signout", null);

        public static Task<HttpResponseMessage> GetModeratorAsync(this HttpClient client, string id)
            => client.GetAsync($"/api/v1/moderators/{id}");

        public static Task<HttpResponseMessage> CreateModeratorAsync(this HttpClient client, string username,
            string password)
        {
            var body = new
            {
                Username = username,
                Password = password
            };

            return client.PostAsJsonAsync("/api/v1/moderators", body);
        }

        public static Task<HttpResponseMessage> GetModeratorListAsync(this HttpClient client)
            => client.GetAsync("/api/v1/moderators");

        public static Task<HttpResponseMessage> DeleteModeratorAsync(this HttpClient client, string id)
            => client.DeleteAsync($"/api/v1/moderators/{id}");
    }
}