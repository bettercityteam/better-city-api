using System;
using System.Net;
using System.Threading.Tasks;
using BetterCity.Domain;
using BetterCity.IntegrationTests.Common;
using BetterCity.Internal.Web.Tests.Fixture;
using FluentAssertions;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Linq;
using Xunit;

namespace BetterCity.Internal.Web.Tests
{
    [Collection("Test")]
    public class ModeratorsApiTests : InternalApiIntegrationTestsBase
    {
        [Fact]
        public async Task Register_new_moderator()
        {
            _ = await Client.SignInAsDefaultAdminAsync();
            
            var response = await Client.CreateModeratorAsync("moderator", "moderator");
            
            response.ShouldBeOk();
            
            var content = await response.Content.ReadAsStringAsync();
            var jobject = JObject.Parse(content);
            jobject.Should().ContainKey("username");
            jobject.Should().ContainKey("id");
        }

        [Fact]
        public async Task Get_moderator()
        {
            _ = await Client.SignInAsDefaultAdminAsync();

            var moderator = await AddModeratorAsync("moderator", "moderator");

            var response = await Client.GetModeratorAsync(moderator.Id);
            
            response.ShouldBeOk();
            
            var content = await response.Content.ReadAsStringAsync();
            var jobj = JObject.Parse(content);
            
            jobj.Should().ContainKey("id").WhichValue.Value<string>().Should().Be(moderator.Id);
            jobj.Should().ContainKey("username").WhichValue.Value<string>().Should().Be(moderator.UserName);
        }

        [Fact]
        public async Task Get_moderators_list()
        {
            _ = await Client.SignInAsDefaultAdminAsync();

            var moderator = AddModeratorAsync("moderator", "moderator");

            var response = await Client.GetModeratorListAsync();

            response.ShouldBeOk();

            var json = await response.Content.ReadAsStringAsync();
            var list = JArray.Parse(json);
            
            list.Should().HaveCount(1);
        }

        [Fact]
        public async Task Delete_moderator()
        {
            _ = await Client.SignInAsDefaultAdminAsync();

            var moderator = await AddModeratorAsync("moderator", "moderator");

            var response = await Client.DeleteModeratorAsync(moderator.Id);

            response.ShouldBeOk();
        }
        
        [Fact]
        public async Task Delete_moderator_that_not_exists()
        {
            _ = await Client.SignInAsDefaultAdminAsync();

            var id = Guid.NewGuid().ToString();
            
            var response = await Client.DeleteModeratorAsync(id);

            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }
        
        private async Task<IdentityUser> AddModeratorAsync(string username, string password)
        {
            using var scope = Factory.Services.CreateScope();
            var manager = scope.ServiceProvider.GetRequiredService<UserManager<IdentityUser>>();
            var moderator = new IdentityUser(username);
            var result = await manager.CreateAsync(moderator, password);
            result.Succeeded.Should().BeTrue();
            var addedToRole = await manager.AddToRoleAsync(moderator, Roles.Moderator);
            addedToRole.Succeeded.Should().BeTrue();

            return moderator;
        }
    }
}