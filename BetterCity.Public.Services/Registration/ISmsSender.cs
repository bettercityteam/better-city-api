using System.Threading.Tasks;

namespace BetterCity.Public.Services.Registration
{
    public interface ISmsSender
    {
        Task SendAsync(string receiver, string message);
    }
}