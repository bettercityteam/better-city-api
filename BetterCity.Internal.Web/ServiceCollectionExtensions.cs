using BetterCity.Internal.Web.Features.CommonModels;
using BetterCity.Internal.Web.Features.Places;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace BetterCity.Internal.Web
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddMappings(this IServiceCollection services)
        {
            return services.AddAutoMapper(config =>
            {
                config.AddProfile(new CommonModelsMappingProfile());
                config.AddProfile(new PlacesMappingProfile());
            });
        }
    }
}