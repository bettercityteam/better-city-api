using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using BetterCity.Domain.Exceptions;
using MediatR;
using Microsoft.AspNetCore.Identity;

namespace BetterCity.Internal.Web.Features.Auth
{
    public static class SignIn
    {
        public class SignInCommand : IRequest
        {
            [Required]
            public string Username { get; set; }
            
            [Required]
            public string Password { get; set; }
        }

        public class Handler : IRequestHandler<SignInCommand>
        {
            private readonly SignInManager<IdentityUser> _signInManager;

            public Handler(SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager)
            {
                _signInManager = signInManager;
            }
            
            public async Task<Unit> Handle(SignInCommand request, CancellationToken cancellationToken)
            {
                var result = await _signInManager.PasswordSignInAsync(
                    request.Username, 
                    request.Password, 
                    true, 
                    false);
                
                if (result.IsNotAllowed)
                    throw new SignInNotAllowedException();

                if (result.IsLockedOut)
                    throw new SignInLockedOutException();

                if (!result.Succeeded)
                    throw new SignInFailedException();
                
                return Unit.Value;
            }
        }
    }
}