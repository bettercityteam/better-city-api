using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;

namespace BetterCity.Internal.Web.Features.Auth
{
    public static class SignOut
    {
        public class Command : IRequest
        {
            
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly SignInManager<IdentityUser> _manager;

            public Handler(SignInManager<IdentityUser> manager)
            {
                _manager = manager;
            }
            
            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                await _manager.SignOutAsync();
                
                return Unit.Value;
            }
        }
    }
}