using System.Threading.Tasks;
using BetterCity.Common.Web.Controllers;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BetterCity.Internal.Web.Features.Auth
{
    [ApiVersion("1.0")]
    public class AuthController : BetterCityApiController
    {
        private readonly IMediator _mediator;

        public AuthController(IMediator mediator)
        {
            _mediator = mediator;
        }
        
        [HttpPost("signin")]
        public async Task SignIn([FromBody] SignIn.SignInCommand command)
        {
            await _mediator.Send(command);
        }

        [Authorize]
        [HttpPost("signout")]
        public new async Task SignOut()
        {
            await _mediator.Send(new SignOut.Command());
        }
    }
}