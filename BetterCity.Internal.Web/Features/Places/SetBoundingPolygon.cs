using System;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BetterCity.Domain;
using BetterCity.Domain.Exceptions;
using BetterCity.Internal.Web.Features.CommonModels;
using BetterCity.Services.Places;
using MediatR;

namespace BetterCity.Internal.Web.Features.Places
{
    public static class SetBoundingPolygon
    {
        public class SetImageBoundingPolygonRequest
        {
            [Required]
            public PointDto[] BoundingPolygon { get; set; }
        }

        public class Command : IRequest
        {
            public Guid Id { get; set; }
            
            public SetImageBoundingPolygonRequest Payload { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly IPlaceGeometryRepository _geometryRepository;
            private readonly IMapper _mapper;

            public Handler(IPlaceGeometryRepository geometryRepository, IMapper mapper)
            {
                _geometryRepository = geometryRepository;
                _mapper = mapper;
            }
            
            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var geometry = await _geometryRepository.GetAsync(request.Id);

                if (geometry == null)
                    throw new ResourceNotFoundException();

                geometry.BoundingPolygon = _mapper.Map<Point[]>(request.Payload.BoundingPolygon);

                await _geometryRepository.UpdateAsync(geometry);
                
                return Unit.Value;
            }
        }
    }
}