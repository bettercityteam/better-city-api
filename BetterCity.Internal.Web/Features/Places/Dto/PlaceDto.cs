using BetterCity.Internal.Web.Features.CommonModels;

namespace BetterCity.Internal.Web.Features.Places.Dto
{
    public record PlaceDto(string Id, string Name, string Summary, CategoryDto[] Categories);
}