using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using BetterCity.Domain;
using BetterCity.Services.Places;
using MediatR;

namespace BetterCity.Internal.Web.Features.Places
{
    public static class Create
    {
        public class CreatePlaceCommand : IRequest<CreatePlaceResult>
        {
            public string Name { get; set; }
            
            public string Summary { get; set; }
        }

        public record CreatePlaceResult(string Id);
        
        public class Handler : IRequestHandler<CreatePlaceCommand, CreatePlaceResult>
        {
            private readonly IPlaceRepository _placeRepository;
            private readonly IPlaceGeometryRepository _geometryRepository;

            public Handler(IPlaceRepository placeRepository, IPlaceGeometryRepository geometryRepository)
            {
                _placeRepository = placeRepository;
                _geometryRepository = geometryRepository;
            }
            
            public async Task<CreatePlaceResult> Handle(CreatePlaceCommand request, CancellationToken cancellationToken)
            {
                var place = new Place
                {
                    Name = request.Name,
                    Summary = request.Summary,
                    Categories = new List<PlaceCategory>()
                };

                var id = await _placeRepository.AddAsync(place);

                var geometry = new PlaceGeometry
                {
                    PlaceId = id,
                    Waypoints = new List<Waypoint>()
                };
                await _geometryRepository.CreateAsync(geometry);
                
                return new(id.ToString());
            }
        }
    }
}