using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BetterCity.Internal.Web.Features.CommonModels;
using BetterCity.Internal.Web.Features.Places.Dto;
using BetterCity.Services.Places;
using MediatR;

namespace BetterCity.Internal.Web.Features.Places
{
    public static class GetById
    {
        public class Query : IRequest<PlaceDto>
        {
            public Guid Id { get; set; }
        }

        public class Handler : IRequestHandler<Query, PlaceDto>
        {
            private readonly IPlaceRepository _repository;
            private readonly IMapper _mapper;

            public Handler(IPlaceRepository repository, IMapper mapper)
            {
                _repository = repository;
                _mapper = mapper;
            }
            
            public async Task<PlaceDto> Handle(Query request, CancellationToken cancellationToken)
            {
                var place = await _repository.GetByIdAsync(request.Id);

                return _mapper.Map<PlaceDto>(place);
            }
        }
    }
}