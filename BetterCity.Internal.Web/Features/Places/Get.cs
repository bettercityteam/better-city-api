using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BetterCity.Internal.Web.Features.Places.Dto;
using BetterCity.Services.Places;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace BetterCity.Internal.Web.Features.Places
{
    public static class Get
    {
        public class Query : IRequest<GetPlacesResponse>
        {
        }

        public class GetPlacesResponse
        {
            public IReadOnlyCollection<PlaceDto> Items { get; set; }

            public int Count => Items.Count;
        }

        public class Handler : IRequestHandler<Query, GetPlacesResponse>
        {
            private readonly IPlaceRepository _placeRepository;
            private readonly IMapper _mapper;

            public Handler(IPlaceRepository placeRepository, IMapper mapper)
            {
                _placeRepository = placeRepository;
                _mapper = mapper;
            }
            
            public Task<GetPlacesResponse> Handle(Query request, CancellationToken cancellationToken)
            {
                var places = _placeRepository.Get().ToList();
                var dtos = _mapper.Map<List<PlaceDto>>(places);
                var response = new GetPlacesResponse {Items = dtos};
                return Task.FromResult(response);
            }
        }
    }
}