using System;
using System.Threading;
using System.Threading.Tasks;
using BetterCity.Domain.Exceptions;
using BetterCity.Services.Places;
using MediatR;

namespace BetterCity.Internal.Web.Features.Places
{
    public static class Delete
    {
        public class Command : IRequest
        {
            public Guid Id { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly IPlaceRepository _placeRepository;
            private readonly IPlaceGeometryRepository _placeGeometryRepository;

            public Handler(IPlaceRepository placeRepository, IPlaceGeometryRepository placeGeometryRepository)
            {
                _placeRepository = placeRepository;
                _placeGeometryRepository = placeGeometryRepository;
            }
            
            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var place = await _placeRepository.GetByIdAsync(request.Id);
                if (place == null)
                    throw new ResourceNotFoundException();

                await _placeGeometryRepository.DeleteAsync(place.Id);

                await _placeRepository.DeleteAsync(place);
                
                return Unit.Value;
            }
        }
    }
}