using System;
using System.Threading;
using System.Threading.Tasks;
using BetterCity.Domain;
using BetterCity.Domain.Exceptions;
using BetterCity.Services.Places;
using MediatR;

namespace BetterCity.Internal.Web.Features.Places.Waypoints
{
    public static class Add
    {
        public class Command : IRequest<AddPlaceWaypointResponse>
        {
            public Guid PlaceId { get; set; }
            
            public AddPlaceWaypointRequest Payload { get; set; }
        }
        
        public class AddPlaceWaypointRequest 
        {
            public double RadiusMeters { get; set; }
        
            public string Name { get; set; }
            
            public double Longitude { get; set; }
        
            public double Latitude { get; set; }
        }

        public record AddPlaceWaypointResponse(Guid Id);

        public class Handler : IRequestHandler<Command, AddPlaceWaypointResponse>
        {
            private readonly IPlaceGeometryRepository _geometryRepository;

            public Handler(IPlaceGeometryRepository geometryRepository)
            {
                _geometryRepository = geometryRepository;
            }
            
            public async Task<AddPlaceWaypointResponse> Handle(Command request, CancellationToken cancellationToken)
            {
                var (latitude, longitude, name, radiusMeters) = 
                    (request.Payload.Latitude, request.Payload.Longitude,
                    request.Payload.Name, request.Payload.RadiusMeters);
                var geometry = await _geometryRepository.GetAsync(request.PlaceId);
                if (geometry == null)
                    throw new ResourceNotFoundException();

                var waypoint = new Waypoint
                {
                    Id = Guid.NewGuid(),
                    Latitude = latitude,
                    Longitude = longitude,
                    Name = name,
                    RadiusMeters = radiusMeters
                };
                geometry.Waypoints.Add(waypoint);

                await _geometryRepository.UpdateAsync(geometry);

                return new AddPlaceWaypointResponse(waypoint.Id);
            }
        }
    }
}