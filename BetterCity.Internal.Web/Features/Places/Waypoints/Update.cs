using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BetterCity.Domain.Exceptions;
using BetterCity.Services.Places;
using MediatR;

namespace BetterCity.Internal.Web.Features.Places.Waypoints
{
    public static class Update
    {
        public class Command : IRequest
        {
            public Guid PlaceId { get; set; }
            
            public Guid WaypointId { get; set; }
            
            public UpdateWaypointRequest Payload { get; set; }
        }

        public class UpdateWaypointRequest
        {
            public string Name { get; set; }
            
            public double RadiusMeters { get; set; }
            
            public double Longitude { get; set; }
        
            public double Latitude { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly IPlaceGeometryRepository _placeGeometryRepository;

            public Handler(IPlaceGeometryRepository placeGeometryRepository)
            {
                _placeGeometryRepository = placeGeometryRepository;
            }
            
            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var geometry = await _placeGeometryRepository.GetAsync(request.PlaceId);
                if (geometry == null)
                    throw new ResourceNotFoundException();

                var waypoint = geometry.Waypoints.FirstOrDefault(w => w.Id == request.WaypointId);
                if (waypoint == null)
                    throw new ResourceNotFoundException();

                var update = request.Payload;
                
                waypoint.Name = update.Name;
                waypoint.RadiusMeters = update.RadiusMeters;
                waypoint.Longitude = update.Longitude;
                waypoint.Latitude = update.Latitude;

                await _placeGeometryRepository.UpdateAsync(geometry);
                
                return Unit.Value;
            }
        }
    }
}