using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BetterCity.Domain.Exceptions;
using BetterCity.Internal.Web.Features.CommonModels;
using BetterCity.Services;
using BetterCity.Services.Places;
using MediatR;
using Microsoft.AspNetCore.Http;

namespace BetterCity.Internal.Web.Features.Places.Categories
{
    public static class SetImage
    {
        public class Command : IRequest<SetCategoryImageResponse>
        {
            public Guid PlaceId { get; set; }
            
            public Guid CategoryId { get; set; }
            
            public Func<Stream> ImageContentFactory { get; set; }
        }

        public class UploadFileRequest
        {
            public IFormFile File { get; set; }
        }
        
        public record SetCategoryImageResponse(CategoryDto Category);
        
        public class Handler : IRequestHandler<Command, SetCategoryImageResponse>
        {
            private readonly IPlaceRepository _placeRepository;
            private readonly IFileUploader _fileUploader;
            private readonly IMapper _mapper;

            public Handler(IPlaceRepository placeRepository, IFileUploader fileUploader, IMapper mapper)
            {
                _placeRepository = placeRepository;
                _fileUploader = fileUploader;
                _mapper = mapper;
            }
            
            public async Task<SetCategoryImageResponse> Handle(Command request, CancellationToken cancellationToken)
            {
                var place = await _placeRepository.GetByIdAsync(request.PlaceId);
                if (place == null)
                    throw new ResourceNotFoundException();

                var category = place.Categories.FirstOrDefault(c => c.Id == request.CategoryId);
                if (category == null)
                    throw new ResourceNotFoundException();

                await using var content = request.ImageContentFactory();
                var imageId = await _fileUploader.UploadAsync(content);
                
                category.ImageId = imageId.ToString();

                await _placeRepository.UpdateAsync(place);

                return new SetCategoryImageResponse(_mapper.Map<CategoryDto>(category));
            }
        }
    }
}