using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BetterCity.Domain.Exceptions;
using BetterCity.Services.Places;
using MediatR;

namespace BetterCity.Internal.Web.Features.Places.Categories
{
    public static class Update
    {
        public class Command : IRequest
        {
            public Guid PlaceId { get; set; }
            
            public Guid CategoryId { get; set; }
            
            public UpdateCategoryRequest Payload { get; set; }
        }

        public class UpdateCategoryRequest
        {
            public string Name { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly IPlaceRepository _placeRepository;

            public Handler(IPlaceRepository placeRepository)
            {
                _placeRepository = placeRepository;
            }
            
            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var place = await _placeRepository.GetByIdAsync(request.PlaceId);
                if (place == null)
                    throw new ResourceNotFoundException();
                
                var category = place.Categories.FirstOrDefault(p => p.Id == request.CategoryId);
                if (category == null)
                    throw new ResourceNotFoundException();

                category.Name = request.Payload.Name;

                await _placeRepository.UpdateAsync(place);
                
                return Unit.Value;
            }
        }
    }
}