using System;
using System.Threading;
using System.Threading.Tasks;
using BetterCity.Domain;
using BetterCity.Services.Places;
using MediatR;

namespace BetterCity.Internal.Web.Features.Places.Categories
{
    public static class Add
    {
        public class Command : IRequest<AddCategoryResponse>
        {
            public Guid PlaceId { get; set; }
            
            public AddCategoryRequest Payload { get; set; }
        }

        public record AddCategoryRequest(string Name);

        public record AddCategoryResponse(Guid Id);

        public class Handler : IRequestHandler<Command, AddCategoryResponse>
        {
            private readonly IPlaceRepository _placeRepository;

            public Handler(IPlaceRepository placeRepository)
            {
                _placeRepository = placeRepository;
            }
            
            public async Task<AddCategoryResponse> Handle(Command request, CancellationToken cancellationToken)
            {
                var place = await _placeRepository.GetByIdAsync(request.PlaceId);

                var category = new PlaceCategory
                {
                    Id = Guid.NewGuid(),
                    Name = request.Payload.Name
                };
                
                place.Categories.Add(category);

                await _placeRepository.UpdateAsync(place);

                return new AddCategoryResponse(category.Id);
            }
        }
    }
}