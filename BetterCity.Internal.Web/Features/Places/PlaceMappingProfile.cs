using AutoMapper;
using BetterCity.Domain;
using BetterCity.Internal.Web.Features.CommonModels;
using BetterCity.Internal.Web.Features.Places.Dto;

namespace BetterCity.Internal.Web.Features.Places
{
    public class PlacesMappingProfile : Profile
    {
        public PlacesMappingProfile()
        {
            CreateMap<PlaceGeometry, GetGeometry.PlaceGeometryDto>();
            CreateMap<Place, PlaceDto>();
            CreateMap<PlaceCategory, CategoryDto>();
        }
    }
}