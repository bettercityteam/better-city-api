using System;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BetterCity.Domain.Exceptions;
using BetterCity.Internal.Web.Features.CommonModels;
using BetterCity.Services.Places;
using MediatR;

namespace BetterCity.Internal.Web.Features.Places
{
    public static class GetGeometry
    {
        public class Query : IRequest<PlaceGeometryDto>
        {
            [Required]
            public Guid PlaceId { get; set; }
        }
        
        public record PlaceGeometryDto(PointDto[] BoundingPolygon, WaypointDto[] Waypoints);
        
        public class Handler : IRequestHandler<Query, PlaceGeometryDto>
        {
            private readonly IPlaceGeometryRepository _repository;
            private readonly IMapper _mapper;

            public Handler(IPlaceGeometryRepository repository, IMapper mapper)
            {
                _repository = repository;
                _mapper = mapper;
            }
            
            public async Task<PlaceGeometryDto> Handle(Query request, CancellationToken cancellationToken)
            {
                var geometry = await _repository.GetAsync(request.PlaceId);

                return _mapper.Map<PlaceGeometryDto>(geometry);
            }
        }
    }
}