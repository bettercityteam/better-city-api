using System;
using System.Threading.Tasks;
using BetterCity.Common.Web.Controllers;
using BetterCity.Domain;
using BetterCity.Internal.Web.Features.Places.Dto;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Add = BetterCity.Internal.Web.Features.Places.Waypoints.Add;
using Update = BetterCity.Internal.Web.Features.Places.Waypoints.Update;

namespace BetterCity.Internal.Web.Features.Places
{
    [ApiVersion("1.0")]
    [Authorize(Roles = Roles.Moderator)]
    public class PlacesController : BetterCityApiController
    {
        private readonly IMediator _mediator;

        public PlacesController(IMediator mediator)
        {
            _mediator = mediator;
        }
        
        [HttpPost]
        public async Task<Create.CreatePlaceResult> Create([FromBody] Create.CreatePlaceCommand command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task Delete(Guid id)
        {
            await _mediator.Send(new Delete.Command {Id = id});
        }

        [HttpGet]
        public async Task<Get.GetPlacesResponse> Get()
        {
            return await _mediator.Send(new Get.Query());
        }
        
        [HttpGet("{id:guid}")]
        public async Task<PlaceDto> GetById(Guid id)
        {
            return await _mediator.Send(new GetById.Query {Id = id});
        }

        [HttpGet("{id:guid}/geometry")]
        public async Task<GetGeometry.PlaceGeometryDto> GetGeometry(Guid id)
        {
            return await _mediator.Send(new GetGeometry.Query {PlaceId = id});
        }

        [HttpPut("{id:guid}/bounding")]
        public async Task SetBoundingPolygon(Guid id, [FromBody] SetBoundingPolygon.SetImageBoundingPolygonRequest request)
        {
            var command = new SetBoundingPolygon.Command {Id = id, Payload = request};

            await _mediator.Send(command);
        }

        [HttpPost("{id:guid}/waypoints")]
        public async Task<Add.AddPlaceWaypointResponse> AddWaypoint(Guid id, [FromBody] Add.AddPlaceWaypointRequest request)
        {
            var command = new Add.Command {PlaceId = id, Payload = request};
            return await _mediator.Send(command);
        }

        [HttpDelete("{placeId:guid}/waypoints/{waypointId:guid}")]
        public async Task DeleteWaypoint(Guid placeId, Guid waypointId)
        {
            var command = new Features.Places.Waypoints.Delete.Command {PlaceId = placeId, WaypointId = waypointId};
            await _mediator.Send(command);
        }

        [HttpPut("{placeId:guid}/waypoints/{waypointId:guid}")]
        public async Task UpdateWaypoint(Guid placeId, Guid waypointId,
            [FromBody] Update.UpdateWaypointRequest request)
        {
            var command = new Update.Command
            {
                PlaceId = placeId,
                WaypointId = waypointId,
                Payload = request
            };

            await _mediator.Send(command);
        }

        [HttpPost("{placeId:guid}/categories")]
        public async Task<Categories.Add.AddCategoryResponse> AddCategory(
            Guid placeId, 
            [FromBody] Categories.Add.AddCategoryRequest request)
        {
            var command = new Categories.Add.Command
            {
                PlaceId = placeId,
                Payload = request
            };
            
            return await _mediator.Send(command);
        }

        [HttpDelete("{placeId:guid}/categories/{categoryId:guid}")]
        public async Task DeleteCategory(Guid placeId, Guid categoryId)
        {
            var command = new Categories.Delete.Command {PlaceId = placeId, CategoryId = categoryId};
            await _mediator.Send(command);
        }

        [HttpPut("{placeId:guid}/categories/{categoryId:guid}")]
        public async Task UpdateCategory(Guid placeId, Guid categoryId, [FromBody] Categories.Update.UpdateCategoryRequest request)
        {
            var command = new Categories.Update.Command {PlaceId = placeId, CategoryId = categoryId, Payload = request};
            await _mediator.Send(command);
        }

        [HttpPut("{placeId:guid}/categories/{categoryId:guid}/image")]
        public async Task<Categories.SetImage.SetCategoryImageResponse> SetCategoryImage(Guid placeId, Guid categoryId, [FromForm] Categories.SetImage.UploadFileRequest request)
        {
            var command = new Categories.SetImage.Command
            {
                PlaceId = placeId,
                CategoryId = categoryId,
                ImageContentFactory = request.File.OpenReadStream
            };

            return await _mediator.Send(command);
        }
    }
}