using System;
using System.Threading.Tasks;
using BetterCity.Common.Web.Controllers;
using BetterCity.Domain;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;

namespace BetterCity.Internal.Web.Features.Images
{
    [ApiVersion("1.0")]
    [Authorize(Roles = Roles.Moderator)]
    public class ImagesController : BetterCityApiController
    {
        private readonly IMediator _mediator;

        public ImagesController(IMediator mediator)
        {
            _mediator = mediator;
        }
        
        [HttpGet("{id:guid}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var result = await _mediator.Send(new Get.Query {ImageId = id});

            return PngFile(result.File);
        }

        private IActionResult PngFile(IFileInfo file) => File(file.CreateReadStream(), "image/png");
    }
}