using System;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using BetterCity.Domain.Exceptions;
using MediatR;
using Microsoft.Extensions.FileProviders;

namespace BetterCity.Internal.Web.Features.Images
{
    public static class Get
    {
        public class Query : IRequest<Result>
        {
            [Required]
            public Guid ImageId { get; set; }
        }

        public class Result
        {
            public IFileInfo File { get; set; }
        }

        public class Handler : IRequestHandler<Query, Result>
        {
            private readonly IFileProvider _fileProvider;

            public Handler(IFileProvider fileProvider)
            {
                _fileProvider = fileProvider;
            }
            
            public Task<Result> Handle(Query request, CancellationToken cancellationToken)
            {
                var file = _fileProvider.GetFileInfo(request.ImageId.ToString());
                
                if (!file.Exists)
                    throw new ResourceNotFoundException();

                return Task.FromResult(new Result {File = file});
            }
        }
    }
}