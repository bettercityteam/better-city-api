using System.Collections.Generic;
using System.Threading.Tasks;
using BetterCity.Common.Web.Controllers;
using BetterCity.Domain;
using BetterCity.Internal.Web.Features.CommonModels;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BetterCity.Internal.Web.Features.Moderators
{
    [Authorize(Roles = Roles.Admin)]
    [ApiVersion("1.0")]
    public class ModeratorsController : BetterCityApiController
    {
        private readonly IMediator _mediator;

        public ModeratorsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<UserDto> Register([FromBody] Register.ModeratorRegisterCommand command)
        {
            return await _mediator.Send(command);
        }

        [HttpGet("{id}")]
        public async Task<UserDto> Get(string id)
        {
            return await _mediator.Send(new Get.Query {Id = id});
        }

        [HttpGet]
        public async Task<IReadOnlyCollection<UserDto>> GetList()
        {
            return await _mediator.Send(new GetList.Query());
        }

        [HttpDelete("{id}")]
        public async Task Delete(string id)
        {
            await _mediator.Send(new Delete.Command {Id = id});
        }
    }
}