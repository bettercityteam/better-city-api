using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BetterCity.Domain;
using BetterCity.Domain.Exceptions;
using BetterCity.Internal.Web.Features.CommonModels;
using MediatR;
using Microsoft.AspNetCore.Identity;

namespace BetterCity.Internal.Web.Features.Moderators
{
    public static class Get
    {
        public class Query : IRequest<UserDto>
        {
            public string Id { get; set; }
        }

        public class Handler : IRequestHandler<Query, UserDto>
        {
            private readonly UserManager<IdentityUser> _userManager;
            private readonly IMapper _mapper;

            public Handler(UserManager<IdentityUser> userManager, IMapper mapper)
            {
                _userManager = userManager;
                _mapper = mapper;
            }
            
            public async Task<UserDto> Handle(Query request, CancellationToken cancellationToken)
            {
                var user = await _userManager.FindByIdAsync(request.Id);

                if (user == null)
                    throw new UserNotFoundException();
                
                var isModerator = await _userManager.IsInRoleAsync(user, Roles.Moderator);
                
                if (!isModerator)
                    throw new UserNotFoundException();

                return _mapper.Map<UserDto>(user);
            }
        }
    }
}