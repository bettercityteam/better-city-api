using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BetterCity.Domain;
using BetterCity.Internal.Web.Features.CommonModels;
using MediatR;
using Microsoft.AspNetCore.Identity;

namespace BetterCity.Internal.Web.Features.Moderators
{
    public static class GetList
    {
        public class Query : IRequest<IReadOnlyCollection<UserDto>>
        {
            
        }

        public class Handler : IRequestHandler<Query, IReadOnlyCollection<UserDto>>
        {
            private readonly UserManager<IdentityUser> _userManager;
            private readonly IMapper _mapper;

            public Handler(UserManager<IdentityUser> userManager, IMapper mapper)
            {
                _userManager = userManager;
                _mapper = mapper;
            }
            
            public async Task<IReadOnlyCollection<UserDto>> Handle(Query request, CancellationToken cancellationToken)
            {
                var users = await _userManager.GetUsersInRoleAsync(Roles.Moderator);
                return _mapper.Map<IReadOnlyCollection<UserDto>>(users);
            }
        }
    }
}