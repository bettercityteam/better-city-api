using System;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BetterCity.Domain;
using BetterCity.Domain.Exceptions;
using BetterCity.Employee.Infrastructure;
using BetterCity.Internal.Web.Features.CommonModels;
using MediatR;
using Microsoft.AspNetCore.Identity;

namespace BetterCity.Internal.Web.Features.Moderators
{
    public static class Register
    {
        
        public class ModeratorRegisterCommand : IRequest<UserDto>
        {
            [Required]
            public string Username { get; set; }
            
            [Required]
            public string Password { get; set; }
        }

        public class Handler : IRequestHandler<ModeratorRegisterCommand, UserDto>
        {
            private readonly UserManager<IdentityUser> _manager;
            private readonly BetterCityEmployeeDbContext _context;
            private readonly IMapper _mapper;

            public Handler(UserManager<IdentityUser> manager, BetterCityEmployeeDbContext context, IMapper mapper)
            {
                _manager = manager;
                _context = context;
                _mapper = mapper;
            }
            
            public async Task<UserDto> Handle(ModeratorRegisterCommand request, CancellationToken cancellationToken)
            {
                await using var transaction = await _context.Database.BeginTransactionAsync(cancellationToken);
                try
                {
                    var moderator = new IdentityUser(request.Username);
                    var result = await _manager.CreateAsync(moderator, request.Password);

                    if (!result.Succeeded)
                        throw new RegistrationFailedException();

                    var rolesResult = await _manager.AddToRoleAsync(moderator, Roles.Moderator);

                    if (!rolesResult.Succeeded)
                        throw new RegistrationFailedException();

                    await transaction.CommitAsync(cancellationToken);
                    
                    return _mapper.Map<UserDto>(moderator);
                }
                catch (Exception)
                {
                    await transaction.RollbackAsync(cancellationToken);
                    throw;
                }
            }
        }
    }
}