using System;
using System.Threading;
using System.Threading.Tasks;
using BetterCity.Domain;
using BetterCity.Domain.Exceptions;
using MediatR;
using Microsoft.AspNetCore.Identity;

namespace BetterCity.Internal.Web.Features.Moderators
{
    public static class Delete
    {
        public class Command : IRequest
        {
            public string Id { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly UserManager<IdentityUser> _manager;

            public Handler(UserManager<IdentityUser> manager)
            {
                _manager = manager;
            }
            
            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var user = await _manager.FindByIdAsync(request.Id);
                if (user == null)
                    throw new UserNotFoundException();

                var isModerator = await _manager.IsInRoleAsync(user, Roles.Moderator);

                if (!isModerator)
                    throw new UserNotFoundException();
                
                var result = await _manager.DeleteAsync(user);

                if (!result.Succeeded)
                    throw new Exception();
                
                return Unit.Value;
            }
        }
    }
}