namespace BetterCity.Internal.Web.Features.CommonModels
{
    public record WaypointDto(double Longitude, double Latitude, string Name, double RadiusMeters);
}