namespace BetterCity.Internal.Web.Features.CommonModels
{
    public record UserDto(
        string Id,
        string Username);
}