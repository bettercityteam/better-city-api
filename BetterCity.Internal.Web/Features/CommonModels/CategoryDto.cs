namespace BetterCity.Internal.Web.Features.CommonModels
{
    public record CategoryDto(string Id, string Name, string ImageId);
}