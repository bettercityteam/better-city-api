using System;
using AutoMapper;
using BetterCity.Domain;
using Microsoft.AspNetCore.Identity;

namespace BetterCity.Internal.Web.Features.CommonModels
{
    public class CommonModelsMappingProfile : Profile
    {
        public CommonModelsMappingProfile()
        {
            CreateMap<IdentityUser, UserDto>()
                .ConvertUsing(user => new UserDto(user.Id, user.UserName));

            CreateMap<Point, PointDto>();
            CreateMap<Waypoint, WaypointDto>();
            CreateMap<Guid, string>().ConvertUsing(guid => guid.ToString());
        }
    }
}