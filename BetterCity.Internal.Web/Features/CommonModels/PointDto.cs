namespace BetterCity.Internal.Web.Features.CommonModels
{
    public record PointDto(double Longitude, double Latitude);
}