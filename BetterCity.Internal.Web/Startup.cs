using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using BetterCity.Common.Web;
using BetterCity.Common.Web.Middleware;
using BetterCity.Employee.Infrastructure;
using BetterCity.Places.Infrastructure;
using BetterCity.Services.Places;
using MediatR;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Serilog;

namespace BetterCity.Internal.Web
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            // TODO separate services registration

            services.AddMappings();
            services.AddTransient<DataSeeder>();
            services.AddDbContext<BetterCityEmployeeDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("Employee")));
            services.AddIdentity<IdentityUser, IdentityRole>().AddEntityFrameworkStores<BetterCityEmployeeDbContext>()
                .AddRoles<IdentityRole>();
            services.Configure<IdentityOptions>(Configuration.GetSection(nameof(IdentityOptions)));
            services.AddPlacesServices(Configuration);
            services.ConfigureApplicationCookie(options => options.Events = new CookieAuthenticationEvents
            {
                OnRedirectToAccessDenied = context =>
                {
                    context.HttpContext.Response.StatusCode = (int) HttpStatusCode.Forbidden;
                    return Task.CompletedTask;
                }
            });
            services.AddMediatR(typeof(Startup).Assembly);
            services.AddControllers();
            services.AddSwaggerGen();
            
            services.AddCors(options =>
                options.AddPolicy("AllowCors", builder =>
                    builder
                        .AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod()));

            services.AddApiVersioning(options => { options.ReportApiVersions = true; }).AddVersionedApiExplorer(
                options =>
                {
                    options.GroupNameFormat = "'v'VVV";
                    options.SubstituteApiVersionInUrl = true;
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IMapper mapper)
        {
            mapper.ConfigurationProvider.AssertConfigurationIsValid();
            app.UseSerilogRequestLogging();
            app.UseBetterCityExceptionHandler();
            app.UseExceptionLogger();
            
            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint(SwaggerConstants.JsonPath, "Better city API"); });

            app.UseRouting();
            app.UseCors("AllowCors");
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}