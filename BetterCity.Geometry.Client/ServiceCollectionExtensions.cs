using System;
using System.Collections.Generic;
using BetterCity.Geometry.Client.Places;
using BetterCity.Geometry.Client.Waypoints;
using Microsoft.Extensions.DependencyInjection;
using Refit;

namespace BetterCity.Geometry.Client
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddGeometryRestClient(
            this IServiceCollection services,
            Action<IHttpClientBuilder>? config = null)
        {
            var settings = new RefitSettings()
            {
                ContentSerializer = new NewtonsoftJsonContentSerializer()
            };
            
            IHttpClientBuilder[] httpClientBuilders =
            {
                services.AddRefitClient<IPlacesClient>(settings), 
                services.AddRefitClient<IWaypointsClient>(settings)
            };

            if (config == null) return services;
            
            foreach (var builder in httpClientBuilders)
            {
                config(builder);
            }

            return services;
        }
    }
}