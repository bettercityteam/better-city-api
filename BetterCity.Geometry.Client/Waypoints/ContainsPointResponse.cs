namespace BetterCity.Geometry.Client.Waypoints
{
    public class ContainsPointResponse
    {
        public bool Result { get; set; }
    }
}