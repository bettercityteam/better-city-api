namespace BetterCity.Geometry.Client.Waypoints
{
    public class ContainsPointRequest
    {
        public decimal[] PointCoordinates { get; set; } = default!;
    }
}