namespace BetterCity.Geometry.Client.Waypoints
{
    public class CreateRequest
    {
        public string Id { get; set; } = default!;

        public string PlaceId { get; set; } = default!;

        public string Name { get; set; } = default!;

        public decimal[] PointCoordinates { get; set; } = default!;
            
        public decimal Radius { get; set; }
    }
}