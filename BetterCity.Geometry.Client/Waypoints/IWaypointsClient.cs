using System.Threading.Tasks;
using Refit;

namespace BetterCity.Geometry.Client.Waypoints
{
    public interface IWaypointsClient
    {
        [Post("/api/waypoints")]
        public Task Create([Body] CreateRequest request);

        [Post("/api/waypoints/{waypointId}/contains-point")]
        public Task<ContainsPointResponse> ContainsPoint(string waypointId, [Body] ContainsPointRequest request);
    }
}