namespace BetterCity.Geometry.Client.Places
{
    public class GetNearestRequest
    {
        public decimal[] Coordinate { get; set; } = default!;

        public decimal Radius { get; set; }
    }
}