using System.Collections.Generic;

namespace BetterCity.Geometry.Client.Places
{
    public class GetNearestResponse
    {
        public IReadOnlyCollection<string> Places { get; set; } = new List<string>();
    }
}