namespace BetterCity.Geometry.Client.Places
{
    public class ContainsPointRequest
    {
        public string PlaceId { get; set; } = default!;

        public decimal[] PointCoordinates { get; set; } = default!;
    }
}