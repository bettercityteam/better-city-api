namespace BetterCity.Geometry.Client.Places
{
    public class CreateRequest
    {
        public string Id { get; set; } = default!;

        public string CuratorId { get; set; } = default!;

        public string Description { get; set; } = default!;

        public string Name { get; set; } = default!;

        public decimal[][] Coordinates { get; set; } = default!;
    }
}