using System.Threading.Tasks;
using Refit;

namespace BetterCity.Geometry.Client.Places
{
    public interface IPlacesClient
    {
        [Post("/api/places")]
        public Task Create([Body] CreateRequest request);

        [Post("/api/places/nearest")]
        public Task<GetNearestResponse> GetNearest([Body] GetNearestRequest request);
 
        [Post("/api/places/{placeId}/contains-point")]
        public Task<ContainsPointResponse> ContainsPoint(string placeId, [Body] ContainsPointRequest request);
    }
}