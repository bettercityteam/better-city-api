namespace BetterCity.Geometry.Client.Places
{
    public class ContainsPointResponse
    {
        public bool Result { get; set; }
    }
}