using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BetterCity.User.Infrastructure
{
    public class BetterCityUserDbContext : IdentityDbContext
    {
        public BetterCityUserDbContext(DbContextOptions<BetterCityUserDbContext> options)
        : base(options)
        {
            
        }
    }
}