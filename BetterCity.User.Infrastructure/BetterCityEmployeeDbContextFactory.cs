using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace BetterCity.User.Infrastructure
{
    public class BetterCityUserDbContextFactory : IDesignTimeDbContextFactory<BetterCityUserDbContext>
    {
        public BetterCityUserDbContext CreateDbContext(string[] args)
        {
            if (args.Length == 0)
                args = new[] {"dbmigration.settings.json"};

            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(args[0])
                .Build();

            var builder = new DbContextOptionsBuilder<BetterCityUserDbContext>();
            builder.UseSqlServer(configuration.GetConnectionString("Default"));
            return new BetterCityUserDbContext(builder.Options);
        }
    }
}