using System.Threading.Tasks;
using BetterCity.Public.Services.Registration;
using Microsoft.Extensions.Options;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace BetterCity.User.Infrastructure
{
    public class TwilioSmsSender : ISmsSender
    {
        private readonly Options _options;
        
        public TwilioSmsSender(IOptions<Options> options)
        {
            _options = options.Value;
        }
        
        public async Task SendAsync(string receiver, string message)
        {
            TwilioClient.Init(_options.AccountSid, _options.AuthToken);

            await MessageResource.CreateAsync(
                body: message,
                from: _options.TwilioPhone,
                to: receiver);
        }

        public class Options
        {
            public string TwilioPhone { get; set; }
            
            public string AccountSid { get; set; }
            
            public string AuthToken { get; set; }
        }
    }
}