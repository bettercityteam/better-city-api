namespace BetterCity.Common.Web
{
    public static class SwaggerConstants
    {
        public const string JsonPath = "/swagger/v1/swagger.json";
        public const string ApiPath = "/swagger";
    }
}