using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace BetterCity.Common.Web.Controllers
{
    [ApiController]
    [Route("api/v{version:apiVersion}/[controller]")]
    [EnableCors("AllowCors")]
    public class BetterCityApiController : ControllerBase
    {
        
    }
}