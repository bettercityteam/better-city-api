using System.Net;
using Microsoft.AspNetCore.Http;

namespace BetterCity.Common.Web
{
    public static class HttpContextExtensions
    {
        public static HttpContext SetResponseCode(this HttpContext context, int statusCode)
        {
            context.Response.StatusCode = statusCode;
            return context;
        }

        public static HttpContext SetResponseCode(this HttpContext context, HttpStatusCode statusCode)
            => context.SetResponseCode((int) statusCode);

        public static HttpContext Unauthorized(this HttpContext context)
            => context.SetResponseCode(HttpStatusCode.Unauthorized);

        public static HttpContext Forbidden(this HttpContext context)
            => context.SetResponseCode(HttpStatusCode.Forbidden);

        public static HttpContext MethodNotAllowed(this HttpContext context)
            => context.SetResponseCode(HttpStatusCode.MethodNotAllowed);

        public static HttpContext BadRequest(this HttpContext context)
            => context.SetResponseCode(HttpStatusCode.BadRequest);
        
        public static HttpContext NotFound(this HttpContext context)
            => context.SetResponseCode(HttpStatusCode.NotFound);
        
        public static HttpContext NotImplemented(this HttpContext context)
            => context.SetResponseCode(HttpStatusCode.NotImplemented);
        
        public static HttpContext InternalServerError(this HttpContext context)
            => context.SetResponseCode(HttpStatusCode.InternalServerError);
    }
}