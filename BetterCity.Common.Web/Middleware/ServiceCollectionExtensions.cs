using Microsoft.AspNetCore.Builder;

namespace BetterCity.Common.Web.Middleware
{
    public static class ServiceCollectionExtensions
    {
        public static IApplicationBuilder UseBetterCityExceptionHandler(this IApplicationBuilder app)
        {
            return app.UseMiddleware<BetterCityExceptionHandlingMiddleware>();
        }

        public static IApplicationBuilder UseExceptionLogger(this IApplicationBuilder app)
        {
            return app.UseMiddleware<ExceptionLoggingMiddleware>();
        }
    }
}