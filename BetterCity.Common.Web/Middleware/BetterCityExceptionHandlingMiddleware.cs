using System;
using System.Net;
using System.Threading.Tasks;
using BetterCity.Domain.Exceptions;
using Microsoft.AspNetCore.Http;

namespace BetterCity.Common.Web.Middleware
{
    public class BetterCityExceptionHandlingMiddleware
    {
        private readonly RequestDelegate _next;

        public BetterCityExceptionHandlingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception e)
            {
                switch (e)
                {
                    case SignInFailedException:
                        context.Unauthorized();
                        break;
                    case CodeVerificationFailedException:
                    case SignInLockedOutException:
                        context.Forbidden();
                        break;
                    case SignInNotAllowedException:
                        context.MethodNotAllowed();
                        break;
                    case InvalidDataFormatException:
                    case InvalidCodeSendingChannelException:
                    case RegistrationFailedException:
                        context.BadRequest();
                        break;
                    case ResourceNotFoundException:
                    case UserNotFoundException:
                        context.NotFound();
                        break;
                    case NotImplementedException:
                        context.NotImplemented();
                        break;
                    default:
                        context.InternalServerError();
                        break;
                }
            }
        }
    }
}