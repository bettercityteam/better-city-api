# better-city-api

Better city web API.

## Internal API

Internal API provides tools for administrators and moderators to manage employees, places and reviews.

## Public API 

Public API provides methods for users (clients) to create reviews, get nearby places, register and authenticate.

## How to run

Build the solution and run `docker-compose.yml` to deploy infrastructure. Then you can run `BetterCity.Public.Web` (Public API) and `BetterCity.Internal.Web` (Internal API).

For Public API you should specify [Twilio](http://twilio.com) credentials in `appsettings.Development.json`. Just replace `TwilioSmsSender` section fields with your credentials.
