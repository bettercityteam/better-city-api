using System.IO;
using BetterCity.Domain;
using BetterCity.Services;
using BetterCity.Services.Places;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.FileProviders;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;

namespace BetterCity.Places.Infrastructure
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddPlacesServices(
            this IServiceCollection services,
            IConfiguration configuration)
            => services
                .AddMongoPlacesRepository(configuration)
                .AddFileServices();

        private static IServiceCollection AddMongoPlacesRepository(this IServiceCollection services,
            IConfiguration configuration)
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(Place)))
            {
                BsonClassMap.RegisterClassMap<Place>(classMap =>
                {
                    classMap.AutoMap();
                    classMap.MapIdProperty(place => place.Id).SetIdGenerator(GuidGenerator.Instance);
                });

                BsonClassMap.RegisterClassMap<PlaceGeometry>(classMap =>
                {
                    classMap.AutoMap();
                    classMap.MapIdProperty(geometry => geometry.Id).SetIdGenerator(GuidGenerator.Instance);
                });

                BsonClassMap.RegisterClassMap<PlaceCategory>(classMap =>
                {
                    classMap.AutoMap();
                    classMap.MapIdProperty(category => category.Id).SetIdGenerator(GuidGenerator.Instance);
                });
            }

            services.TryAddScoped<IPlaceRepository, MongoPlaceRepository>();
            services.Configure<MongoPlaceRepository.Options>(configuration.GetSection(nameof(MongoPlaceRepository)));
            services.TryAddScoped<IPlaceGeometryRepository, MongoPlaceGeometryRepository>();
            services.Configure<MongoPlaceGeometryRepository.Options>(
                configuration.GetSection(nameof(MongoPlaceGeometryRepository)));
            return services;
        }

        private static IServiceCollection AddFileServices(this IServiceCollection services)
        {
            var root = Path.Combine(Directory.GetCurrentDirectory(), "storage");
            services.AddScoped<IFileProvider, PhysicalFileProvider>(_ => new PhysicalFileProvider(root));
            services.AddScoped<IFileUploader, FileUploader>();
            services.Configure<FileUploader.Options>(options => options.RootPath = root);
            return services;
        }
    }
}