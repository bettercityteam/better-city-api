using System;
using System.IO;
using System.Threading.Tasks;
using BetterCity.Services;
using Microsoft.Extensions.Options;

namespace BetterCity.Places.Infrastructure
{
    public class FileUploader : IFileUploader
    {
        private readonly Options _options;
        
        public FileUploader(IOptions<Options> options)
        {
            _options = options.Value;
        }

        public async Task<Guid> UploadAsync(Stream content)
        {
            var guid = Guid.NewGuid();
            var path = Path.Combine(_options.RootPath, guid.ToString());
            if (!Directory.Exists(_options.RootPath))
            {
                Directory.CreateDirectory(_options.RootPath);
            }
            await using var file = File.Create(path);
            await content.CopyToAsync(file);
            return guid;
        }
        
        public class Options
        {
            public string RootPath { get; set; }
        }
    }
}