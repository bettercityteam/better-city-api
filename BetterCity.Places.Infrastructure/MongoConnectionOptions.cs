namespace BetterCity.Places.Infrastructure
{
    public class MongoConnectionOptions
    {
        public string ConnectionString { get; set; }
            
        public string CollectionName { get; set; }
            
        public string DatabaseName { get; set; }
    }
}