using System;
using System.Threading.Tasks;
using BetterCity.Domain;
using BetterCity.Services.Places;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace BetterCity.Places.Infrastructure
{
    public class MongoPlaceGeometryRepository : IPlaceGeometryRepository
    {
        private readonly IMongoCollection<PlaceGeometry> _placeGeometries;
        
        public MongoPlaceGeometryRepository(IOptions<Options> options)
        {
            var optionsValue = options.Value;
            var client = new MongoClient(optionsValue.ConnectionString);
            var database = client.GetDatabase(optionsValue.DatabaseName);
            _placeGeometries = database.GetCollection<PlaceGeometry>(optionsValue.CollectionName);
        }
        
        public async Task<PlaceGeometry> GetAsync(Guid placeId)
        {
            return await _placeGeometries.Find(geometry => geometry.PlaceId == placeId).FirstOrDefaultAsync();
        }

        public async Task CreateAsync(PlaceGeometry placeGeometry)
        {
            await _placeGeometries.InsertOneAsync(placeGeometry);
        }

        public async Task UpdateAsync(PlaceGeometry placeGeometry)
        {
            await _placeGeometries.ReplaceOneAsync(geometry => geometry.Id == placeGeometry.Id, placeGeometry);
        }

        public Task DeleteAsync(PlaceGeometry placeGeometry)
        {
            return DeleteAsync(placeGeometry.PlaceId);
        }

        public async Task DeleteAsync(Guid placeId)
        {
            await _placeGeometries.DeleteOneAsync(geometry => geometry.PlaceId == placeId);
        }

        public class Options : MongoConnectionOptions
        {
        }
    }
}