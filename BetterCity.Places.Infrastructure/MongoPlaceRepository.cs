using System;
using System.Linq;
using System.Threading.Tasks;
using BetterCity.Domain;
using BetterCity.Services.Places;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace BetterCity.Places.Infrastructure
{
    public class MongoPlaceRepository : IPlaceRepository
    {
        private readonly IMongoCollection<Place> _places;
        
        public MongoPlaceRepository(IOptions<Options> options)
        {
            var optionsValue = options.Value;
            var client = new MongoClient(optionsValue.ConnectionString);
            var database = client.GetDatabase(optionsValue.DatabaseName);
            _places = database.GetCollection<Place>(optionsValue.CollectionName);
        }
        
        public async Task<Guid> AddAsync(Place place)
        {
            await _places.InsertOneAsync(place);

            return place.Id;
        }

        public async Task<Place> GetByIdAsync(Guid id)
        {
            return await _places.Find(place => place.Id == id).FirstOrDefaultAsync();
        }

        public IQueryable<Place> Get()
        {
            return _places.AsQueryable();
        }

        public async Task UpdateAsync(Place place)
        {
            await _places.FindOneAndReplaceAsync(p => p.Id == place.Id, place);
        }

        public Task DeleteAsync(Place place)
        {
            return DeleteAsync(place.Id);
        }

        public async Task DeleteAsync(Guid id)
        {
            await _places.DeleteOneAsync(place => place.Id == id);
        }

        public class Options : MongoConnectionOptions
        {
            
        }
    }
}