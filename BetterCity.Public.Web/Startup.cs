using System;
using AutoMapper;
using BetterCity.Common.Web;
using BetterCity.Common.Web.Middleware;
using BetterCity.Geometry.Client;
using BetterCity.Geoserver.AspNetCore.Proxy;
using BetterCity.Public.Services.Registration;
using BetterCity.User.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;

namespace BetterCity.Public.Web
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            // TODO separate services registration
            services.AddStackExchangeRedisCache(Configuration.GetSection("Redis").Bind);
            
            services.AddScoped<ISmsSender, TwilioSmsSender>();
            services.Configure<TwilioSmsSender.Options>(Configuration.GetSection(nameof(TwilioSmsSender)));
            
            services.Configure<Features.Registration.SendCode.Handler.SendRegistrationCodeOptions>
                (Configuration.GetSection(nameof(Features.Registration.SendCode.Handler.SendRegistrationCodeOptions)));

            services.Configure<Features.Auth.SendCode.Handler.SendAuthCodeOptions>(
                Configuration.GetSection(nameof(Features.Auth.SendCode.Handler.SendAuthCodeOptions)));
            
            services.AddMappings();
            
            services.AddDbContext<BetterCityUserDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("Users")));
            services.AddIdentity<IdentityUser, IdentityRole>().AddEntityFrameworkStores<BetterCityUserDbContext>().AddRoles<IdentityRole>();
            services.Configure<IdentityOptions>(Configuration.GetSection(nameof(IdentityOptions)));
            
            services.AddMediatR(typeof(Startup).Assembly);
            
            services.AddControllers();
            services.AddSwaggerGen();
            
            services.AddCors(options => 
                options.AddPolicy("AllowCors", builder => 
                    builder
                        .AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod()));
            
            services.AddApiVersioning(options =>
            {
                options.ReportApiVersions = true;
            }).AddVersionedApiExplorer(options =>
            {
                options.GroupNameFormat = "'v'VVV";
                options.SubstituteApiVersionInUrl = true;
            });
            var geometryBaseAddress = Configuration["BetterCity:Geometry:BaseAddress"];
            services.AddGeometryRestClient(builder => builder.ConfigureHttpClient(c => c.BaseAddress = new Uri(geometryBaseAddress)));
            services.Configure<GeoserverProxyOptions>(Configuration.GetSection("BetterCity:GeoserverProxy"));
            services.AddGeoserverProxy();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IMapper mapper)
        {
            mapper.ConfigurationProvider.AssertConfigurationIsValid();
            
            app.UseSerilogRequestLogging();
            app.UseBetterCityExceptionHandler();
            app.UseExceptionLogger();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint(SwaggerConstants.JsonPath, "Better city API");
            });
            
            app.UseRouting();
            app.UseCors("AllowCors");
            app.UseAuthentication();
            app.UseAuthorization();
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            app.UseGeoserverProxy();
        }
    }
}