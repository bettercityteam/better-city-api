using BetterCity.Public.Web.Features.Places;
using Microsoft.Extensions.DependencyInjection;

namespace BetterCity.Public.Web
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddMappings(this IServiceCollection services)
        {
            return services.AddAutoMapper(config =>
            {
                config.AddProfile(new PlacesMappingProfile());
            });
        }
    }
}