using System.Threading.Tasks;
using BetterCity.Common.Web.Controllers;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace BetterCity.Public.Web.Features.Places
{
    [ApiVersion("1.0")]
    public class PlacesController : BetterCityApiController
    {
        private readonly IMediator _mediator;

        public PlacesController(IMediator mediator)
        {
            _mediator = mediator;
        }
        
        [HttpPost("get-nearest")]
        public Task<GetNearest.GetNearestPlacesResponse> GetNearest(GetNearest.GetNearestPlacesQuery query)
        {
            return _mediator.Send(query);
        }
    }
}