using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BetterCity.Geometry.Client.Places;
using MediatR;

namespace BetterCity.Public.Web.Features.Places
{
    public class GetNearest
    {
        public record GetNearestPlacesResponse(IReadOnlyCollection<string> Places);
        
        public class GetNearestPlacesQuery : IRequest<GetNearestPlacesResponse>
        {
            public decimal[] Coordinate { get; set; }

            public decimal Radius { get; set; }
        }
        
        public class Handler : IRequestHandler<GetNearestPlacesQuery, GetNearestPlacesResponse>
        {
            private readonly IPlacesClient _placesClient;
            private readonly IMapper _mapper;

            public Handler(IPlacesClient placesClient, IMapper mapper)
            {
                _placesClient = placesClient;
                _mapper = mapper;
            }

            public async Task<GetNearestPlacesResponse> Handle(GetNearestPlacesQuery query,
                CancellationToken cancellationToken)
            {
                var request = _mapper.Map<GetNearestRequest>(query);
                var clientResponse = await _placesClient.GetNearest(request);
                return _mapper.Map<GetNearestPlacesResponse>(clientResponse);
            }
        }
    }
}