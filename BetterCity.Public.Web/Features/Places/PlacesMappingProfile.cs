
using AutoMapper;
using BetterCity.Geometry.Client.Places;

namespace BetterCity.Public.Web.Features.Places
{
    public class PlacesMappingProfile : Profile
    {
        public PlacesMappingProfile()
        {
            CreateMap<GetNearest.GetNearestPlacesQuery, GetNearestRequest>();
            CreateMap<GetNearestResponse, GetNearest.GetNearestPlacesResponse>();
        }
    }
}