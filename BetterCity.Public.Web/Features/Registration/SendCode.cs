using System;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using BetterCity.Domain.Exceptions;
using BetterCity.Domain.Services;
using BetterCity.Public.Web.Features.Registration.Messages;
using MediatR;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Options;

namespace BetterCity.Public.Web.Features.Registration
{
    public static class SendCode
    {
        private static readonly Regex PhoneMatcher = new Regex("^\\+7[0-9]{10}$");

        public class RegistrationSendCodeCommand : IRequest
        {
            public string Phone { get; set; }
        }
        public class Handler : IRequestHandler<RegistrationSendCodeCommand>
        {
            private readonly IMediator _mediator;
            private readonly IDistributedCache _cache;
            private readonly SendRegistrationCodeOptions _options;

            public Handler(IMediator mediator, IDistributedCache cache, IOptions<SendRegistrationCodeOptions> options)
            {
                _mediator = mediator;
                _cache = cache;
                _options = options.Value;
            }
            
            public async Task<Unit> Handle(RegistrationSendCodeCommand request, CancellationToken cancellationToken)
            {
                var code = VerificationCodeGenerator.Generate();
                await CacheCodeAsync($"registration.code.{request.Phone}", code, cancellationToken);
                
                
                if (IsConsole())
                {
                    Console.WriteLine(code);
                }
                else if (IsPhone(request.Phone))
                {
                    await SendSmsAsync(request.Phone, code);
                }
                else
                {
                    throw new InvalidCodeSendingChannelException();
                }
                
                return Unit.Value;
            }

            private async Task CacheCodeAsync(string key, string code, CancellationToken cancellationToken)
            {
                var options = new DistributedCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(5)
                };
                await _cache.SetStringAsync(key, code, options, cancellationToken);
            }

            private bool IsConsole() => _options.UseConsoleChannel;

            private async Task SendSmsAsync(string phone, string code)
            {
                await _mediator.Publish(new SendCodeViaSmsMessage(phone, code));
            }

            private bool IsPhone(string requestChannel) => PhoneMatcher.IsMatch(requestChannel);
            
            public class SendRegistrationCodeOptions
            {
                public bool UseConsoleChannel { get; set; }
            }
        }
    }
}