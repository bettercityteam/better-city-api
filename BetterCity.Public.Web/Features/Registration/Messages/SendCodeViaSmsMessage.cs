using MediatR;

namespace BetterCity.Public.Web.Features.Registration.Messages
{
    public record SendCodeViaSmsMessage(string Receiver, string Code) : INotification;
}