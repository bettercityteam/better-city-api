using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BetterCity.Domain.Exceptions;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Caching.Distributed;

namespace BetterCity.Public.Web.Features.Registration
{
    public static class RegisterUser
    {
        public class RegisterUserCommand : IRequest
        {
            public string Phone { get; set; }
            
            public string Token { get; set; }
        }
        
        public class Handler : IRequestHandler<RegisterUserCommand>
        {
            private readonly UserManager<IdentityUser> _userManager;
            private readonly SignInManager<IdentityUser> _signInManager;
            private readonly IDistributedCache _cache;

            public Handler(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, IDistributedCache cache)
            {
                _userManager = userManager;
                _signInManager = signInManager;
                _cache = cache;
            }
            
            public async Task<Unit> Handle(RegisterUserCommand request, CancellationToken cancellationToken)
            {
                var expectedToken = await _cache.GetStringAsync($"registration.token.{request.Phone}", cancellationToken);

                if (string.IsNullOrEmpty(expectedToken))
                    throw new RegistrationFailedException();
                
                if (!string.Equals(expectedToken, request.Token))
                    throw new RegistrationFailedException();

                var phone = request.Phone;

                var userExists = _userManager.Users.Any(u => u.PhoneNumber == phone);
                
                if (userExists)
                    throw new RegistrationFailedException();
                
                if (string.IsNullOrEmpty(phone))
                    throw new RegistrationFailedException();
                
                var user = new IdentityUser(request.Token) {PhoneNumber = phone};
                var result = await _userManager.CreateAsync(user);

                if (!result.Succeeded)
                    throw new RegistrationFailedException();

                await _signInManager.SignInAsync(user, true);

                return Unit.Value;
            }
        }
    }
}