using System;
using System.Threading;
using System.Threading.Tasks;
using BetterCity.Domain.Exceptions;
using MediatR;
using Microsoft.Extensions.Caching.Distributed;

namespace BetterCity.Public.Web.Features.Registration
{
    public static class VerifyCode
    {
        public record RegistrationVerifyCodeResponse(string RegistrationToken);

        public class RegistrationVerifyCodeRequest : IRequest<RegistrationVerifyCodeResponse>
        {
            public string Phone { get; set; }
            
            public string Code { get; set; }
        }

        public class Handler : IRequestHandler<RegistrationVerifyCodeRequest, RegistrationVerifyCodeResponse>
        {
            private readonly IDistributedCache _cache;

            public Handler(IDistributedCache cache)
            {
                _cache = cache;
            }
            
            public async Task<RegistrationVerifyCodeResponse> Handle(RegistrationVerifyCodeRequest request, CancellationToken cancellationToken)
            {
                var code = await _cache.GetStringAsync($"registration.code.{request.Phone}", cancellationToken);

                if (string.IsNullOrEmpty(code))
                    throw new CodeVerificationFailedException();

                if (!string.Equals(code, request.Code))
                    throw new CodeVerificationFailedException();

                var registrationToken = Guid.NewGuid().ToString("N").ToUpper();

                var tokenCacheOptions = new DistributedCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(5)
                };
                
                await _cache.SetStringAsync($"registration.token.{request.Phone}", registrationToken, tokenCacheOptions, cancellationToken);
                
                return new RegistrationVerifyCodeResponse(registrationToken);
            }
        }
    }
}