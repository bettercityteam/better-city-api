using System;
using System.Threading;
using System.Threading.Tasks;
using BetterCity.Public.Services.Registration;
using BetterCity.Public.Web.Features.Registration.Messages;
using MediatR;

namespace BetterCity.Public.Web.Features.Registration.MessageHandlers
{
    public class SmsCodeSender : INotificationHandler<SendCodeViaSmsMessage>
    {
        private readonly ISmsSender _sender;

        public SmsCodeSender(ISmsSender sender)
        {
            _sender = sender;
        }
        
        public async Task Handle(SendCodeViaSmsMessage notification, CancellationToken cancellationToken)
        {
            var (receiver, code) = notification;
            await _sender.SendAsync(receiver, code);
        }
    }
}