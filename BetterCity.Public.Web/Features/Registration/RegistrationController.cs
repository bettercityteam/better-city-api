using System.Threading.Tasks;
using BetterCity.Common.Web.Controllers;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace BetterCity.Public.Web.Features.Registration
{
    [ApiVersion("1.0")]
    public class RegistrationController : BetterCityApiController
    {
        private readonly IMediator _mediator;

        public RegistrationController(IMediator mediator)
        {
            _mediator = mediator;
        }
        
        [HttpPost("sendCode")]
        public async Task SendCode([FromBody] SendCode.RegistrationSendCodeCommand command)
        {
            await _mediator.Send(command);
        }

        [HttpPost("verifyCode")]
        public async Task<VerifyCode.RegistrationVerifyCodeResponse> GetRegistrationToken([FromBody] VerifyCode.RegistrationVerifyCodeRequest request)
        {
            return await _mediator.Send(request);
        }

        [HttpPost("register")]
        public async Task Register([FromBody] RegisterUser.RegisterUserCommand command)
        {
            await _mediator.Send(command);
        }
    }
}