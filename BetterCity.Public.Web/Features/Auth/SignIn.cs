using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using BetterCity.Domain.Exceptions;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;

namespace BetterCity.Public.Web.Features.Auth
{
    public static class SignIn
    {
        public class SignInCommand : IRequest
        {
            [Required]
            public string Phone { get; set; }
            
            [Required]
            public string AuthToken { get; set; }
        }

        public class Handler : IRequestHandler<SignInCommand>
        {
            private readonly SignInManager<IdentityUser> _signInManager;
            private readonly IDistributedCache _cache;

            public Handler(SignInManager<IdentityUser> signInManager, IDistributedCache cache)
            {
                _signInManager = signInManager;
                _cache = cache;
            }
            
            public async Task<Unit> Handle(SignInCommand request, CancellationToken cancellationToken)
            {
                var expectedToken = await _cache.GetStringAsync(
                    $"auth.token.{request.Phone}", 
                    cancellationToken);

                if (string.IsNullOrEmpty(expectedToken))
                    throw new SignInFailedException();

                if (!string.Equals(expectedToken, request.AuthToken))
                    throw new SignInFailedException();

                var user = await _signInManager.UserManager.Users
                    .FirstOrDefaultAsync(u => u.PhoneNumber == request.Phone, cancellationToken);

                if (user == null)
                    throw new SignInFailedException();
                
                await _signInManager.SignInAsync(user, true);
                
                return Unit.Value;
            }
        }
    }
}