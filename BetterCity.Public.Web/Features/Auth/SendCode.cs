using System;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using BetterCity.Domain.Exceptions;
using BetterCity.Domain.Services;
using BetterCity.Public.Services.Registration;
using BetterCity.Public.Web.Features.Registration.Messages;
using MediatR;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Options;

namespace BetterCity.Public.Web.Features.Auth
{
    public static class SendCode
    {
        private static readonly Regex PhoneMatcher = new Regex("^\\+7[0-9]{10}$");
        
        public class AuthSendCodeCommand : IRequest
        {
            public string Phone { get; set; }
        }

        public class Handler : IRequestHandler<AuthSendCodeCommand>
        {
            private readonly IDistributedCache _cache;
            private readonly IMediator _mediator;
            private readonly SendAuthCodeOptions _options;

            public Handler(IDistributedCache cache, IMediator mediator, IOptions<SendAuthCodeOptions> options)
            {
                _cache = cache;
                _mediator = mediator;
                _options = options.Value;
            }
            
            public async Task<Unit> Handle(AuthSendCodeCommand request, CancellationToken cancellationToken)
            {
                var code = VerificationCodeGenerator.Generate();
                await CacheCodeAsync($"auth.code.{request.Phone}", code, cancellationToken);
                
                if (IsConsole())
                {
                    Console.WriteLine(code);
                }
                else if (IsPhone(request.Phone))
                {
                    await SendSmsAsync(request.Phone, code);
                }
                else
                {
                    throw new InvalidCodeSendingChannelException();
                }

                return Unit.Value;
            }
            
            private async Task CacheCodeAsync(string key, string code, CancellationToken cancellationToken)
            {
                var options = new DistributedCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(5)
                };
                await _cache.SetStringAsync(key, code, options, cancellationToken);
            }

            private bool IsConsole() => _options.UseConsoleChannel;

            private async Task SendSmsAsync(string phone, string code)
            {
                await _mediator.Publish(new SendCodeViaSmsMessage(phone, code));
            }

            private bool IsPhone(string requestChannel) => PhoneMatcher.IsMatch(requestChannel);
            
            public class SendAuthCodeOptions
            {
                public bool UseConsoleChannel { get; set; }
            }
        }
    }
}