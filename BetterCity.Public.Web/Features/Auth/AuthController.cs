using System.Threading.Tasks;
using BetterCity.Common.Web.Controllers;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BetterCity.Public.Web.Features.Auth
{
    [ApiVersion("1.0")]
    public class AuthController : BetterCityApiController
    {
        private readonly IMediator _mediator;

        public AuthController(IMediator mediator)
        {
            _mediator = mediator;
        }
        
        [HttpPost("sendCode")]
        public async Task SendCode([FromBody] SendCode.AuthSendCodeCommand command)
        {
            await _mediator.Send(command);
        }

        [HttpPost("verifyCode")]
        public async Task<VerifyCode.AuthVerifyCodeResponse> VerifyCode(
            [FromBody] VerifyCode.AuthVerifyCodeRequest request)
        {
            return await _mediator.Send(request);
        }

        [HttpPost("signIn")]
        public async Task SignIn([FromBody] SignIn.SignInCommand command)
        {
            await _mediator.Send(command);
        }

        [Authorize]
        [HttpPost("signOut")]
        public new async Task SignOut()
        {
            await _mediator.Send(new SignOut.Command());
        }
    }
}