using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;

namespace BetterCity.Public.Web.Features.Auth
{
    public static class SignOut
    {
        public class Command : IRequest
        {
            
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly SignInManager<IdentityUser> _signInManager;

            public Handler(SignInManager<IdentityUser> signInManager)
            {
                _signInManager = signInManager;
            }
            
            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                await _signInManager.SignOutAsync();
                
                return Unit.Value;
            }
        }
    }
}