using System;
using System.Threading.Tasks;
using BetterCity.Domain;

namespace BetterCity.Services.Places
{
    public interface IPlaceGeometryRepository
    {
        public Task<PlaceGeometry> GetAsync(Guid placeId);

        public Task CreateAsync(PlaceGeometry placeGeometry);

        public Task UpdateAsync(PlaceGeometry placeGeometry);

        public Task DeleteAsync(PlaceGeometry placeGeometry);

        public Task DeleteAsync(Guid placeId);
    }
}