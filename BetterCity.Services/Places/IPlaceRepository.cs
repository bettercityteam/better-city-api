using System;
using System.Linq;
using System.Threading.Tasks;
using BetterCity.Domain;

namespace BetterCity.Services.Places
{
    public interface IPlaceRepository
    {
        public Task<Guid> AddAsync(Place place);

        public Task<Place> GetByIdAsync(Guid id);

        public IQueryable<Place> Get();

        public Task UpdateAsync(Place place);

        public Task DeleteAsync(Place place);

        public Task DeleteAsync(Guid id);
    }
}