using System;
using System.IO;
using System.Threading.Tasks;

namespace BetterCity.Services
{
    public interface IFileUploader
    {
        public Task<Guid> UploadAsync(Stream content);
    }
}